/*
 * print_message.hpp
 *
 *  Created on: 16 марта 2015
 *      Author: strangeman
 */

#ifndef PRINT_MESSAGE_HPP_
#define PRINT_MESSAGE_HPP_

#include <iostream>
#include <cassert>

enum MESSAGE_ID {
	WARN_UNEXP_OEL, WARN_UNEXP_CHAR, WARN_WRONG_ID, WARN_EMPTY_LINE, WARN_UNEXP_CHARS_UNTIL_EOL, ERR_BAD_FILE, ERR_FILE_NON_SPECIFIED
};

enum FILE_ID {
	ONE_OF = -1, FIRST_SOURCE_FILE = 0, SECOND_SOURCE_FILE, RESULT_FILE
};

void PrintMessage ( MESSAGE_ID _m, std::ostream & _stream, int _fid = ONE_OF, const int _lineNumber = -1 );

#endif /* PRINT_MESSAGE_HPP_ */
