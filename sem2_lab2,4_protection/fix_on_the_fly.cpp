/*
 * fix_on_the_fly.cpp
 *
 *  Created on: May 31, 2015
 *      Author: betatester
 */

#include "parsers.hpp"
#include "dict.hpp"

void FixOnTheFly( const Dict::Dictionary & _dict, std::istream & _input,  std::ostream & _output) {
	CharVector::ChVector tempVector;
	CharVector::Init( tempVector );
	const char * tempWordPtr;

	while ( true ) {
		if ( _input.eof() )
			break;

		Parsers::ProxyNotAlphas( _input, _output );

		if ( _input.eof() )
			break;

		CharVector::Clear( tempVector );
		Parsers::ReadWord( tempVector, _input );
		tempWordPtr = Dict::GetSimilar( _dict, tempVector.m_pData );
		if (tempWordPtr == nullptr)
			_output << tempVector.m_pData;
		else
			_output << tempWordPtr;
	}

	CharVector::Destroy( tempVector );
}


