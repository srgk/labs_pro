/*
 * dict.hpp
 *
 *  Created on: May 31, 2015
 *      Author: betatester
 */

#ifndef DICT_HPP_
#define DICT_HPP_

#include <iostream>

namespace Dict {

enum PartOfSpeech {
	UNDEF_PART = -1, NOUN, VERB, ADJECTIVE, ADVERB, PRONOUN, PREPOSITION, CONJUNCTION, INTERJECTION, ARTICLE, N_OF_CLASSES
};

// VFNUM - num of verb forms; FNUM - num of possible word numbers
enum Form {
	UNDEF_FORM = -1, SINGULAR, PLURAL, NFNUM, VING = 0, V1, V2, V3, VFNUM
};

/* В m_examples содержатся все возможные формы слова.
 * Структура данного массива зависит от класса слова
 * Если слово - глагол, то структура будет иметь вид:
 * { [первая форма, ед. число] [2 форма, ед.ч.] [3 ф., ед.ч.] [+ing, ед.ч.] [1 ф., множ.ч.] [2 ф., множ.ч.] [3 ф., мн.ч.] [+ing, мн.ч.]  }
 * Если слово - имя существительное:
 * { [ед. число] [множ.ч.] }
 * Для всех остальных:
 * { [слово] }
 */
struct DictWord {
	char ** m_examples = nullptr;
	PartOfSpeech m_class = UNDEF_PART;
};

struct Dictionary {
	DictWord * m_pData = nullptr;
	int m_nUsed = 0;
	int m_nAllocated = 0;
};

void Init (Dictionary & _dict, int _size = 10);

void Destroy (Dictionary & _dict);

void Grow (Dictionary & _dict);

void PushInited (Dictionary & _dict, DictWord & _word);

const char * GetWordForm (const DictWord & _word, const Form _num, const Form _vFrom = VFNUM);

void Read (Dictionary & _dict, std::istream & _stream);

void Print (const Dictionary & _dict, std::ostream & _stream);

const char * GetSimilar (const Dictionary & _dict, const char * _source);

}

#endif /* DICT_HPP_ */
