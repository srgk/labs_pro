/*
 * dict.cpp
 *
 *  Created on: May 31, 2015
 *      Author: betatester
 */

#include "char_vector.hpp"
#include "dict.hpp"
#include <cassert>
#include <string.h>
#include <ctype.h>

namespace Dict {

enum SimilarityLevel {
	 IDENTICAL, SIMILAR, DIFFERENT
};

void Init (Dictionary & _dict, int _size) {
	_dict.m_pData = new DictWord [_size];
	_dict.m_nAllocated = _size;
	_dict.m_nUsed = 0;
}

int GetNOfExamples (PartOfSpeech & _class) {
	int nOfExamples;
	switch (_class) {
		case NOUN: nOfExamples = NFNUM;
		break;

		case VERB: nOfExamples = NFNUM * VFNUM;
		break;

		default: nOfExamples = 1;
	}

	return nOfExamples;
}

void DestroyWord (DictWord & _word) {
	assert (_word.m_examples);

	int nOfExamples = GetNOfExamples (_word.m_class);

	for (int i = 0; i < nOfExamples; i++) {
		assert (_word.m_examples[i]);
		delete[] _word.m_examples[i];
		_word.m_examples[i] = nullptr;
	}

	delete[] _word.m_examples;
	_word.m_examples = nullptr;
}

void Destroy (Dictionary & _dict) {
	for (int i = 0; i < _dict.m_nUsed; i++)
		DestroyWord (_dict.m_pData[i]);

	delete [] _dict.m_pData;

	_dict.m_pData = nullptr;
}

void Grow (Dictionary & _dict) {
	const int newSize = _dict.m_nAllocated * 2;
	Dictionary newDict;
	newDict.m_pData = new DictWord[newSize];
	newDict.m_nAllocated = newSize;
	newDict.m_nUsed = _dict.m_nUsed;
	memcpy( newDict.m_pData, _dict.m_pData, sizeof(DictWord) * _dict.m_nAllocated );
	delete[] _dict.m_pData;
	_dict = newDict;
}

int GetTargetPosition (Dictionary & _dict, const DictWord & _word) {
	int pos = 0;

	while ( pos < _dict.m_nUsed ) {
		if (strcmp( _word.m_examples[0],  _dict.m_pData[pos].m_examples[0]) > 0)
			break;

		++ pos;
	}

	return pos;
}

void ShiftRight (Dictionary & _dict, const int _pos) {
	assert( _dict.m_pData != nullptr );

	if (_dict.m_nUsed == _dict.m_nAllocated)
		Grow( _dict );

	for (int i = _dict.m_nUsed; i >= _pos; i--)
		_dict.m_pData[i + 1] = _dict.m_pData[i];

	++ _dict.m_nUsed;
}

/*
//Если нужен отсортированнный словарь:
void PushInited (Dictionary & _dict, DictWord & _word) {
	assert (_word.m_examples != nullptr);
	assert (_dict.m_pData != nullptr);

	int position = GetTargetPosition( _dict, _word );

	ShiftRight( _dict, position );

	_dict.m_pData[position] = _word;
}
 */

//Если сойдет неотсортированный словарь:
void PushInited (Dictionary & _dict, DictWord & _word) {
	assert (_word.m_examples != nullptr);
	assert (_dict.m_pData != nullptr);

	if (_dict.m_nUsed == _dict.m_nAllocated)
		Grow( _dict );

	_dict.m_pData[ _dict.m_nUsed++ ] = _word;
}

const char * GetWordForm (const DictWord & _word, const Form _num, const Form _vForm) {
	int index;
	switch (_word.m_class) {
		case NOUN: index = _num;
		break;

		/*
		// Дилемма
		// Так - более универсально
		case VERB: index = _num * VFNUM + _vForm;

		// А так - быстрее */
		case VERB: index = (_num << 2) + _vForm;
		break;

		default: index = 0;
	}
	return _word.m_examples[index];
}

PartOfSpeech GetPartOfSpeech (const char * _source) {
	const char * wordClassIds[] = {
		//NOUN, VERB, ADJECTIVE, ADVERB, PRONOUN, PREPOSITION, CONJUNCTION, INTERJECTION, ARTICLE
		"[n]", "[v]", "[adj]", "[adv]", "[pro]", "[prep]", "[conj]", "[i]", "[art]"
	};

	const short nOfClasses = sizeof( wordClassIds ) / sizeof( const char *);
	assert( nOfClasses == N_OF_CLASSES );

	for (int i = 0; i < nOfClasses; i++)
		if  ( strstr(_source, wordClassIds[i]) )
			return static_cast<PartOfSpeech>(i);

	return UNDEF_PART;
}

DictWord * ParseAndGetWord (const CharVector::ChVector & _source) {
	char * sourceCopy = new char [_source.m_nUsed];
	strcpy( sourceCopy, _source.m_pData );

	PartOfSpeech part = GetPartOfSpeech (sourceCopy);

	if ( part == UNDEF_PART)
		return nullptr;

	DictWord * word = new DictWord;
	word->m_class = part;

	const int nOfExamples = GetNOfExamples( word->m_class );

	word->m_examples = new char * [nOfExamples];

	char * temp = sourceCopy;
	//temp = strtok( temp, "]" );
	strtok( temp, " " );
	temp = strtok( NULL, " " );

	for (int i = 0; i < nOfExamples; i++) {
		if (temp == nullptr) {
			DestroyWord( *word );
			return nullptr;
		}
		word->m_examples[i] = new char [sizeof( temp )];
		strcpy( word->m_examples[i], temp );
		temp = strtok( NULL, " " );
	}

	delete[] sourceCopy;
	return word;
}

void Read (Dictionary & _dict, std::istream & _stream) {
	CharVector::ChVector tempString;
	CharVector::Init (tempString);
	DictWord * tempWord;
	for (int line = 0; ! _stream.eof(); line++) {
		CharVector::GetUntil( tempString, _stream, "\n" );
		if (tempString.m_nUsed == 0)
			continue;

		if (tempString.m_pData[0] == '#')
			continue;

		tempWord = ParseAndGetWord( tempString );

		if (tempWord != nullptr) {
			PushInited( _dict, *tempWord );
			delete tempWord;
		}

		CharVector::Clear( tempString );
	}

	CharVector::Destroy( tempString );
}

void PrintWord (const DictWord & _word, std::istream & _stream) {
	assert (! "Not implemented");
}

void Print (const Dictionary & _dict, std::ostream & _stream) {
	assert (! "Not implemented");
}

/*
 * Возвращает
 * -1, если строки отличаются больше, чем на один символ;
 * 0, если строки идентичны;
 * 1, если строки подобны (отличаются на один символ)
 */
SimilarityLevel IsSimilar( const char * _string, const char * _example ) {
	const int slen1st = strlen( _string ), slen2nd = strlen( _example );
	const int sizeDiff = slen1st - slen2nd;

	int countDiff = abs( sizeDiff );

	for (int i = 0, j = 0; (i < slen1st) && (j < slen2nd); i++, j++) {
		if (countDiff > 1)
			return DIFFERENT;

		if (	( _string[i] != _example[j] )
				&&
				( tolower( _string[i] ) != _example[j] )
			)
		{
			countDiff++;

			if ( sizeDiff < 0 )
				i--;

			else if ( sizeDiff > 0 )
				j--;
		}
	}
	if (countDiff && (slen1st < 4 || slen2nd < 4))
		return DIFFERENT;

	return static_cast<SimilarityLevel>(countDiff);
}

const char * GetSimilar (const Dictionary & _dict, const char * _source) {
	assert( _source != nullptr );
	assert( _dict.m_pData != nullptr );

	int nOfExamples;
	SimilarityLevel tempDiff;
	char * lastSimilar = nullptr;

	for ( int i = 0; i < _dict.m_nUsed; i++ ) {
		nOfExamples = GetNOfExamples( _dict.m_pData[i].m_class );
		for ( int j = 0; j < nOfExamples; j++ ) {
			tempDiff = IsSimilar( _source, _dict.m_pData[i].m_examples[j] );

			switch (tempDiff) {
			case IDENTICAL:
				return _source;
				break;

			case SIMILAR:
				lastSimilar = _dict.m_pData[i].m_examples[j];
				break;

			default:
				break;
			}
		}
	}

	return lastSimilar;
}

}
