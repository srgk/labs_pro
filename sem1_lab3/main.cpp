#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void randInit() {
	time_t currentTime;
	time( & currentTime );
	srand( currentTime );
}

int getRand() {
	return (rand() % 100 - 50);
}

int main ()
{
	randInit();
	const int masSize=23;
	int vector[masSize], countNegative=0, multiplicationAccumulator = 1;
	for(int i=0; i < masSize; i++) {
		vector[i] = getRand();
	}
	printf("Vector have elements: ");
	for(int i=0; i < masSize; i++) {
		printf("%d, ", vector[i]);
	}
	printf("\n");
	for(int i=0; i < masSize; i++) {
		if (vector[i] == 0) {
			continue;
		}
		if (vector[i] > 0) {
			if (!(i % 2)) {
				multiplicationAccumulator *= vector[i];
			}
		}
		else {
			countNegative++;
		}
	}
	printf("Vector have %d negative elements and multiplication of positive elements is %d\n", countNegative, multiplicationAccumulator);
	return 0;
}
