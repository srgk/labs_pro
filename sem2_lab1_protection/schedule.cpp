#include "schedule.h"

void initSchedule(Schedule & _sched, int _n = 10) {
	_sched.m_tNumber = _n;
	_sched.m_pUnits = new Transport[_sched.m_tNumber];
}

void destroySchedule(Schedule & _sched) {
	delete [] _sched.m_pUnits;
	_sched.m_pUnits = nullptr;
}
