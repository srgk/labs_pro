/*
 * schedule.h
 *
 *  Created on: 04 марта 2015 г.
 *      Author: betatester
 */

#ifndef SCHEDULE_H_
#define SCHEDULE_H_

#include "dates.h"

struct Transport {
	int number, route;
	Date shipping;
	char city[20];
};

struct Schedule {
	Transport * m_pUnits;
	int m_tNumber;
};

void initSchedule(Schedule & _sched, int _n);

void destroySchedule(Schedule & _sched);

#endif /* SCHEDULE_H_ */
