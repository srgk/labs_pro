#include <time.h>
#include <stdio.h>
#include <string.h>
#include <cassert>

#include "schedule.h"

const char * const CITIES[] = {
		"Kiev", "Kharkiv", "Dnipropetrovsk", "Zaporizhia", "Lviv", "Kryvyi Rih", "Odessa", "Mykolaiv", "Mariupol",  "Makiivka", "Vinnytsia", "Kherson", "Poltava", "Chernihiv", "Cherkasy", "Sumy"
};

const int NUM_OF_CITIES = sizeof CITIES / sizeof CITIES[0];

void fillRandCity(char * _city) {
	assert(NUM_OF_CITIES > 0);

	int i = rand() % NUM_OF_CITIES;

	assert(_city);

	strcpy(_city, CITIES[i]);
}

void scheduleUnit (Transport & _t, int _number) {
	fillRandDate(_t.shipping);
	fillRandCity(_t.city);
	_t.number = _number;
	_t.route = getRandNumber();
}

void fillSchedule(Schedule & _sched) {
	for (int i = 0; i < _sched.m_tNumber; i++) {
		scheduleUnit (_sched.m_pUnits[i], i + 1);
	}
}

void printScheduleUnit(const Schedule & _sched, int _i) {
	assert(_i < _sched.m_tNumber);

	printf(
			"%2d\t%-20s\t", _sched.m_pUnits[_i].number,  _sched.m_pUnits[_i].city
			);
	printDate(_sched.m_pUnits[_i].shipping);
	printf("\n");
}

void printSchedule(const Schedule & _sched) {
	printf("\nNumber\tDestination\t\tShipping date\n");

	for(int i = 0; i < _sched.m_tNumber; i++) {
		printScheduleUnit(_sched, i);
	}
	printf("\n");
}

void plusDayGlobal (Schedule & _sched) {
	for (int i = 0; i < _sched.m_tNumber; i++)
		plusDay(_sched.m_pUnits[i].shipping);
}

bool isGoingToCity (const Transport & _t, const char * _city) {
	return !(strcmp(_t.city, _city));
}

int findIndexByOdessa (const Schedule & _sched) {
	int indexOfEarlier = -1;
	for (int i = 0; i < _sched.m_tNumber; i++) {
		if (isGoingToCity(_sched.m_pUnits[i], "Odessa")) {
			if (indexOfEarlier == -1)
				indexOfEarlier = i;
			if (getDateDiffInDays(_sched.m_pUnits[indexOfEarlier].shipping, _sched.m_pUnits[i].shipping) > 0)
				indexOfEarlier = i;
		}
	}
	return indexOfEarlier;
}

void printFirstToOdessa (const Schedule & _sched) {
	int index = findIndexByOdessa (_sched);
	printf("\nFirst train in year to Odessa:\n");
	if (index == -1) {
		printf("There is no train shipping to Odessa this year\n");
		return;
	}

	printScheduleUnit(_sched, index);
}

int findIndexBy8March (const Schedule & _sched) {
	int indexOfClosest = -1;
	Date march8 = {8, MARCH, 2015};
	for (int i = 0; i < _sched.m_tNumber; i++) {
		if (isGoingToCity(_sched.m_pUnits[i], "Kiev")) {
			if (indexOfClosest == -1) {
				indexOfClosest = i;
				continue;
			}
			if ( getModuleDateDiff(_sched.m_pUnits[indexOfClosest].shipping, march8) >
				getModuleDateDiff(_sched.m_pUnits[i].shipping, march8) ) {
				indexOfClosest = i;
			}
		}
	}
	return indexOfClosest;
}

void printBy8March (const Schedule & _sched) {
	int index = findIndexBy8March (_sched);
	printf("\nClosest to 8 March train to Kiev:\n");
	if (index == -1) {
		printf("There is no train shipping to Kiev this year\n");
		return;
	}

	printScheduleUnit(_sched, index);
}

int main() {
	time_t currentTime;
	time(&currentTime);
	srand(currentTime);

	Schedule sched;

	initSchedule(sched, 30);
	fillSchedule(sched);

	printf("\nFull schedule:");
	printSchedule(sched);

	printFirstToOdessa (sched);

	printBy8March (sched);

	plusDayGlobal(sched);

	printf("\nFull schedule +1 day:");
	printSchedule(sched);

	destroySchedule(sched);

	return 0;
}
