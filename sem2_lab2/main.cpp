/*
 * main.cpp
 *
 *  Created on: 11 апр. 2015
 *      Author: strangeman
 */

#include "get_triangle_matrix.hpp"
#include <iostream>

int main () {
	RealMatrix::RMatrix matrix;
	std::cout << "Input your matrix and press [Enter] twice:\n";
	RealMatrix::Read (matrix, std::cin);
	RealMatrix::MakeTriangleBareiss (matrix);

	std::cout << "This is your triangle matrix:\n";
	RealMatrix::Print (matrix, std::cout, '\t');
	RealMatrix::Destroy (matrix);
	return 0;
}
