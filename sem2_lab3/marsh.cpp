#include <cassert>
#include <string.h>
#include "marsh.h"

namespace MarshSpace {

void initMarsh(MARSH & _m) {
	CharVector::Init(_m.m_source);
	CharVector::Init(_m.m_dest);
}

void initMarshArray(marshArray & _arr, int _size) {
	_arr.m_pMarsh = new MARSH [_size];
	_arr.m_nMax = _size;
	for (int i = 0; i < _size; i++)
		initMarsh(_arr.m_pMarsh[i]);
}

void copyMarsh(MARSH & _to, const MARSH & _from) {
	CharVector::CopyAndReplace(_to.m_source, _from.m_source);
	CharVector::CopyAndReplace(_to.m_dest, _from.m_dest);
	_to.m_routeNum = _from.m_routeNum;
}

void shiftMarshes(marshArray & _arr, int _startIndex) {
	assert(_startIndex != (_arr.m_nMax - 1));
	for (int i = _arr.m_nUsed - 1; i >= _startIndex; i--) {
		copyMarsh(_arr.m_pMarsh[i + 1], (const MARSH)_arr.m_pMarsh[i]);
	}
	_arr.m_nUsed++;
}

void getMarsh(MARSH & _m, std::istream & _stream) {
	_stream >> _m.m_routeNum;
	_stream.get();
	CharVector::Clear(_m.m_source);
	CharVector::Clear(_m.m_dest);
	CharVector::GetUntil(_m.m_source, _stream, "\n\t");
	CharVector::GetUntil(_m.m_dest, _stream, "\n\t");
}

void placeMarshToSortedArray(MARSH & _m, marshArray & _arr) {
	for (int i = 0; i < _arr.m_nUsed; i++) {
		if (_arr.m_pMarsh[i].m_routeNum >= _m.m_routeNum) {
			shiftMarshes(_arr, i);
			copyMarsh(_arr.m_pMarsh[i], (const MARSH)_m);
			return;
		}
	}
	copyMarsh(_arr.m_pMarsh[_arr.m_nUsed++], (const MARSH)_m);
}


void fillMarshArray(marshArray & _arr, std::istream & _stream) {
	MARSH bufMarsh;
	assert(_arr.m_pMarsh[1].m_source.m_pData != nullptr);
	for (int i = 0; i < _arr.m_nMax; i++) {
		getMarsh(bufMarsh, _stream);
		placeMarshToSortedArray(bufMarsh, _arr);
	}
}

void printMarsh(MARSH & _m, std::ostream & _stream) {
	_stream << _m.m_routeNum << "\t" << _m.m_source.m_pData << "\t" << _m.m_dest.m_pData << "\n";
}

void printMarshArray(marshArray & _arr, std::ostream & _stream) {
	_stream << "\nFull route list:\n";
	for (int i = 0; i < _arr.m_nMax; i++) {
		printMarsh(_arr.m_pMarsh[i], _stream);
	}
}

void printMarshPlaceMatch(marshArray & _arr, char * _place, std::ostream & _stream) {
	_stream << "\n";
	bool foundMatch = false;
	for (int i = 0; i < _arr.m_nMax; i++) {
		if (strcmp(_arr.m_pMarsh[i].m_source.m_pData, _place) == 0) {
			printMarsh(_arr.m_pMarsh[i], _stream);
			foundMatch = true;
			continue;
		}
		if (strcmp(_arr.m_pMarsh[i].m_dest.m_pData, _place) == 0) {
			printMarsh(_arr.m_pMarsh[i], _stream);
			foundMatch = true;
			continue;
		}
	}
	if (!foundMatch) {
		_stream << "\nMatch was not found";
	}
}

void destroyMarsh(MARSH & _m) {
	CharVector::Destroy(_m.m_source);
	CharVector::Destroy(_m.m_dest);
}

void destroyMarshArray(marshArray & _arr) {
	for (int i = 0; i < _arr.m_nMax; i++)
		destroyMarsh(_arr.m_pMarsh[i]);

	delete [] _arr.m_pMarsh;
	_arr.m_pMarsh = nullptr;
	_arr.m_nMax = 0;
}
}//end of MarshSpace namespace
