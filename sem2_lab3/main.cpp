/*
Описать структуру с именем MARSH, содержащую следующие поля: название начального пункта маршрута; название конечного пункта маршрута; номер маршрута.
Написать программу, выполняющую следующие действия:
- ввод с клавиатуры данных в массив, состоящий из восьми элементов типа MARSH; записи должны быть упорядочены по номерам маршрутов;
- вывод на экран информации о маршрутах, которые начинаются или оканчиваются в пункте, название которого введено с клавиатуры;
- если таких маршрутов нет, выдать на дисплей соответствующее сообщение.
 */


#include "marsh.h"

int main() {
	int mArrSize = 8;
	MarshSpace::marshArray mArr;
	MarshSpace::initMarshArray(mArr, mArrSize);
	std::cout << "Input bottom properies for " << mArrSize << " routes. Input format is: \n"
			<< "number_of_route [TAB] source_point [TAB] destination_point [NEWLINE]\n";
	MarshSpace::fillMarshArray(mArr);
	MarshSpace::printMarshArray(mArr);
	CharVector::ChVector place;
	CharVector::Init(place);

	std::cout << "\nAnd now input desired place of shipping or arriving: \n";
	CharVector::GetUntil(place);
	std::cout << "\nOK. This routes fit: \n";
	printMarshPlaceMatch(mArr, place.m_pData);
	CharVector::Destroy(place);
	MarshSpace::destroyMarshArray(mArr);

	return 0;
}
