#include <stdio.h>
#include <math.h>

int main()
{
	double x, y, a, b, p;
	x = 1.25;
	y = 0.93;
	a = ((x - y) * ((((x + y) * (x + y)) / ((x + 4)*(x + 4)*(x + 4))) / ((exp(-(x-2))) + (x*x*x + 4))));
	b = (1 + cos(y - 2)) / ((pow (x,(2/4))) + sin(y - 2) * sin(y - 2));
	p = a / b;
	printf("p value is: %lf when x = %lf and y = %lf\n", p, x, y);
	return 0;
}
