#include <iostream>
#include <math.h>

using std::cout;

int main() {
	double y;
	const double hx = 0.1, a = 1.5, b = 2;
	cout << "Solutions of set are:";
	for (double x = 1; x <= 3; x += hx) {

		if (x < 2) {
			y = sin(fabs(a*x + pow(b,a)));
		}
		else {
			y = cos(fabs(a*x - pow(b,a)));
			break;
		}
		cout << " " << y << ";";
	}
	cout << "\n";
	return 0;
}
