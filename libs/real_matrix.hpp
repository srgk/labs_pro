/*
 * real_matrix.hpp
 *
 *  Created on: 11 апр. 2015
 *      Author: strangeman
 */

#ifndef REAL_MATRIX_HPP_
#define REAL_MATRIX_HPP_

#include <iostream>
#include "real_vector.hpp"

namespace RealMatrix  {

	using RealVector::RVector;
	
	struct RMatrix
	{
	    double ** m_pData;
	    int	m_nRowsAllocated;
	    int	m_nRowsUsed = -1;
	    int m_nColumnsUsed;
	    int m_nColumnsAllocated;
	};

	struct ElementPosition2D {
		int x, y;
	};

	enum STATUS {
		OK, ERR_UNEXP_EOL, ERR_BAD_ROW_LENGHT
	};

	void Init ( RMatrix & _matrix, int _nRows, int _nColumns );

	void Destroy ( RMatrix & _matrix );

	void Clear ( RMatrix & _matrix );

	bool IsEmpty ( const RMatrix & _matrix );

	void RowGrow ( RMatrix & _matrix, const int multiplicator = 2 );

	STATUS PushRowBack ( RMatrix & _matrix, const RVector & _dataVector );

	void PopRow ( RMatrix & _matrix );

	void SwapRows ( RMatrix & _matrix, int _firstRowIndex, int _secondRowIndex );

	void InsertRowAt ( RMatrix & _matrix, int _rowIndex, RVector _dataVector );

	void DeleteRowAt ( RMatrix & _matrix, int _rowIndex );

	void DeleteColumnAt ( RMatrix & _matrix, int _columnIndex );

	void MultiplyRow ( RMatrix & _matrix, const int _rowIndex, const double _multiplicator );

	void DivideRow ( RMatrix & _matrix, const int _rowIndex, const double _divider );

	void SumRows ( RMatrix & _matrix, const int _destRowIndex, const int _sourceRowIndex, const double _multiplicator = 1.0 );

	void Read ( RMatrix & _matrix, std::istream & _stream );

	void Print ( const RMatrix & _matrix, std::ostream & _stream, char _sep = ' ' );

} //end of RealMatrix  namespace

#endif /* REAL_MATRIX_HPP_ */
