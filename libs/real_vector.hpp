#ifndef _REAL_VECTOR_HPP_
#define _REAL_VECTOR_HPP_

#include <iostream>

namespace RealVector {
	
	struct RVector
	{
	    double * m_pData;
	    int m_nUsed;
	    int m_nAllocated;
	};


	void Init ( RVector & _vector, int _allocatedSize = 10 );

	void Destroy ( RVector & _vector );

	void Clear ( RVector & _vector );

	bool IsEmpty ( const RVector & _vector );

	void PushBack ( RVector & _vector, double _data );

	void RealVectorPopBack ( RVector & _vector );

	void RealVectorInsertAt ( RVector & _vector, int _position, double _data );

	void RealVectorDeleteAt ( RVector & _vector, int _position );

	void RealVectorRead ( RVector & _vector, std::istream & _stream );

	void RealVectorReadTillZero ( RVector & _vector, std::istream & _stream );

	void RealVectorPrint ( const RVector & _vector, std::ostream & _stream, char _sep = ' ' );

} //end of vectors namespace

#endif //  _REAL_VECTOR_HPP_
