/*
 * real_matrix.cpp
 *
 *  Created on: 11 апр. 2015
 *      Author: strangeman
 */

#include "real_matrix.hpp"
#include <cassert>
#include <string.h>
#include "parsers.hpp"

namespace RealMatrix {

	using RealVector::RVector;
	using RealVector::Clear;
	using RealVector::Destroy;
	using RealVector::Init;

	void Init ( RMatrix & _matrix, int _nRows, int _nColumns ) {
		_matrix.m_pData = new double* [_nRows];

		for (int i = 0; i < _nRows; i++) {
			_matrix.m_pData[i] = new double [_nColumns];
		}

	    _matrix.m_nRowsAllocated = _nRows;
	    _matrix.m_nRowsUsed = 0;
	    _matrix.m_nColumnsAllocated = _nColumns;
	    _matrix.m_nColumnsUsed = 0;
	}

	void Destroy ( RMatrix & _matrix ) {
		for (int i = 0; i < _matrix.m_nRowsAllocated; i++) {
			delete [] _matrix.m_pData[i];
		}

		delete [] _matrix.m_pData;

		_matrix.m_nRowsAllocated = 0;
	}

	void Clear ( RMatrix & _matrix ) {
	    _matrix.m_nRowsUsed = 0;
	    _matrix.m_nColumnsUsed = 0;
	}

	bool IsEmpty ( const RMatrix & _matrix ) {
		return _matrix.m_nRowsUsed == 0;
	}

	void RowGrow ( RMatrix & _matrix, const int multiplicator ) {
		int newRowsAllocated = _matrix.m_nRowsAllocated * multiplicator;

		//Инициализация новой матрицы
		RMatrix newMatrix;

		//Копируем все характеристические поля
		newMatrix = _matrix;

		//Выделяем память под новую матрицу
		newMatrix.m_pData = new double* [newRowsAllocated];

		//Привязываем все строки к новой матрице
		memcpy ( newMatrix.m_pData, _matrix.m_pData, sizeof( double* ) * _matrix.m_nRowsAllocated );

		//Удаляем старую матрицу
		delete [] _matrix.m_pData;

		//Создаем новые строки в новой матрице
		while ( newMatrix.m_nRowsAllocated < newRowsAllocated ) {
			newMatrix.m_pData[ newMatrix.m_nRowsAllocated++ ] = new double [newMatrix.m_nColumnsAllocated];
		}

		//Подменяем:
		_matrix = newMatrix;
	}

	STATUS PushRowBack ( RMatrix & _matrix, const RVector & _dataVector ) {
		if ( _matrix.m_nRowsUsed == -1 )
		{
			Init ( _matrix, 3, _dataVector.m_nUsed );
			_matrix.m_nColumnsUsed = _dataVector.m_nUsed;
		}

		if ( _dataVector.m_nUsed != _matrix.m_nColumnsUsed)
			return ERR_BAD_ROW_LENGHT;

		if ( _matrix.m_nRowsAllocated == _matrix.m_nRowsUsed )
			RowGrow ( _matrix );

		memcpy (_matrix.m_pData [ _matrix.m_nRowsUsed++ ], _dataVector.m_pData, sizeof( double ) * _dataVector.m_nUsed );
		return OK;
	}

	void PopRow ( RMatrix & _matrix ) {
		_matrix.m_nRowsUsed--;
	}

	void SwapRows ( RMatrix & _matrix, int _firstRowIndex, int _secondRowIndex ) {
		assert( (_firstRowIndex >= 0) && (_firstRowIndex < _matrix.m_nRowsUsed));
		assert( (_secondRowIndex >= 0) && (_secondRowIndex < _matrix.m_nRowsUsed));

		double * temp = _matrix.m_pData[_firstRowIndex];
		_matrix.m_pData[_firstRowIndex] = _matrix.m_pData[_secondRowIndex];
		_matrix.m_pData[_secondRowIndex] = temp;
	}

	void InsertRowAt ( RMatrix & _matrix, int _rowIndex, RVector _dataVector ) {
		if ( _matrix.m_nRowsAllocated == _matrix.m_nRowsUsed ) {
			RowGrow ( _matrix );
		}

		assert( (_rowIndex >= 0) && (_rowIndex < _matrix.m_nRowsAllocated));

		double * pFreeRow = _matrix.m_pData[ _matrix.m_nRowsUsed ];

		for (int i = _rowIndex; i < _matrix.m_nRowsUsed; i++) {
			_matrix.m_pData[i + 1] = _matrix.m_pData[i];
		}

		_matrix.m_pData[ _rowIndex ] = pFreeRow;

		memcpy (_matrix.m_pData [ _rowIndex ], _dataVector.m_pData, sizeof( double ) * _dataVector.m_nUsed );

		_matrix.m_nRowsUsed++;
	}

	void DeleteRowAt ( RMatrix & _matrix, int _rowIndex ) {
		assert( (_rowIndex >= 0) && (_rowIndex < _matrix.m_nRowsUsed));
		double * pFreeRow = _matrix.m_pData[ _rowIndex ];

		for (int i = --_matrix.m_nRowsUsed; i > _rowIndex; i--) {
			_matrix.m_pData[i - 1] = _matrix.m_pData[i];
		}

		_matrix.m_pData[ _matrix.m_nRowsUsed ] = pFreeRow;
	}

	void DeleteColumnAt ( RMatrix & _matrix, int _columnIndex ) {
		assert( (_columnIndex >= 0) && (_columnIndex < _matrix.m_nColumnsUsed));

		_matrix.m_nColumnsUsed--;

		for (int i = 0; i < _matrix.m_nRowsUsed; i++)
			for (int j = _matrix.m_nColumnsUsed; j > _columnIndex; j--)
				_matrix.m_pData[i][j - 1] = _matrix.m_pData[i][j];
	}

	void MultiplyRow ( RMatrix & _matrix, const int _rowIndex, const double _multiplicator ) {
		for (int i = 0; i < _matrix.m_nColumnsUsed; i++)
			_matrix.m_pData[_rowIndex][i] *= _multiplicator;
	}

	void DivideRow ( RMatrix & _matrix, const int _rowIndex, const double _divider ) {
		for (int i = 0; i < _matrix.m_nColumnsUsed; i++)
			_matrix.m_pData[_rowIndex][i] /= _divider;
	}

	void SumRows ( RMatrix & _matrix, const int _destRowIndex, const int _sourceRowIndex, const double _multiplicator ) {
		for (int i = 0; i < _matrix.m_nColumnsUsed; i++) {
			_matrix.m_pData [ _destRowIndex ][ i ] +=  _matrix.m_pData [ _sourceRowIndex ][ i ] * _multiplicator;
		}
	}

	void RealVectorReadTillEOL (RVector & _vector, std::istream & _stream) {
		double value;

		while ( Parsers::GetNextReal(value, _stream) ) {
			PushBack (_vector, value);
		}
	}

	void Read ( RMatrix & _matrix, std::istream & _stream ) {
		RVector temp;
		Init(temp);

		do {
			do {
				Clear(temp);
				RealVectorReadTillEOL(temp, _stream);
				if (PushRowBack(_matrix, temp) == ERR_BAD_ROW_LENGHT) {
					std::cout << "Lenght of you row differs from previous rows in matrix, try again:\n";
				}
				else {
					break;
				}
			}
			while (true);

			if ( Parsers::IsEOL (_stream) )
				break;
		}
		while ( ! Parsers::IsEOL (_stream) );

		Destroy(temp);
	}

	void Print ( const RMatrix & _matrix, std::ostream & _stream, char _sep) {
		for (int i = 0; i < _matrix.m_nRowsUsed; i++) {
			for (int j = 0; j < _matrix.m_nColumnsUsed; j++)
				_stream << _matrix.m_pData[i][j] << _sep;

			_stream << "\n";
		}


	}

} //end of RealMatrix namespace
