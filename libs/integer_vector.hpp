#ifndef _INTEGER_VECTOR_HPP_
#define _INTEGER_VECTOR_HPP_

#include <iostream>

namespace IntegerVector {
	
	struct IntVector
	{
	    int * m_pData;
	    int m_nUsed;
	    int m_nAllocated;
	};


	void Init ( IntVector & _vector, int _allocatedSize = 10 );

	void Destroy ( IntVector & _vector );

	void Clear ( IntVector & _vector );

	bool IsEmpty ( const IntVector & _vector );

	void PushBack ( IntVector & _vector, int _data );

	void PopBack ( IntVector & _vector );

	void InsertAt ( IntVector & _vector, int _position, int _data );

	void DeleteAt ( IntVector & _vector, int _position );

	void Read ( IntVector & _vector, std::istream & _stream );

	void ReadTillZero ( IntVector & _vector, std::istream & _stream );

	void Print ( const IntVector & _vector, std::ostream & _stream, char _sep = ' ' );

} //end of IntegerVector namespace

#endif //  _INTEGER_VECTOR_HPP_
