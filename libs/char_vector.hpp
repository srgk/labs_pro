#ifndef CHAR_VECTOR_H_
#define CHAR_VECTOR_H_

#include <iostream>

namespace CharVector {
struct ChVector {
	int m_nAllocated = 0, m_nUsed = 0;
	char * m_pData = nullptr;
};

void Init (ChVector & _s, int _size = 10);

void Destroy (ChVector & _s);

void Clear (ChVector & _s);

void Grow (ChVector & _s, int _sizeRequested = -1);

void PushBack ( ChVector & _s, const char _ch );

void GetUntil (ChVector & _s, std::istream & _stream = std::cin, const char * _terminator = "\n");

bool IsDiffer (const ChVector & _one, const ChVector & _two);

void Print (const ChVector & _s, std::ostream & _stream = std::cout, char _separator = '\n');

void CopyAndReplace (ChVector & _dest, const ChVector & _source);

void CopyAndReplace (ChVector & _dest, const char * _source);
} //end of CharVector namespace

#endif /* CHAR_VECTOR_H_ */
