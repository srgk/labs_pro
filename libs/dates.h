/*
 * dates.h
 *
 *  Created on: 04 марта 2015 г.
 *      Author: betatester
 */

#ifndef DATES_H_
#define DATES_H_

#include <stdlib.h>
#include <stdio.h>

enum MONTH {
	JANUARY = 1, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
};

const int maxDaysInMonth[13] = {
		0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

struct Date {
	int day;
	MONTH month;
	int year;
};

int getRandNumber();

bool isLeapYear(int _year);

int getMaxDays(MONTH _m, int _year);

void printDate (Date & _date);

void fillRandDate(Date & _date);

void plusDay (Date & _date);

long getDateInDaysWithoutYears(Date _d);

int getDateDiffInDays(Date _d1, Date _d2);

int getModuleDateDiff(Date _d1, Date _d2);

#endif /* DATES_H_ */
