/*
 * get_triangle_matrix.cpp
 *
 *  Created on: 11 апр. 2015
 *      Author: strangeman
 */

#include "get_triangle_matrix.hpp"
#include <cmath>

namespace RealMatrix {

	//Реализация алгоритма Барейса
	void MakeTriangleBareiss (RMatrix & _matrix) {

		const int nRows = _matrix.m_nRowsUsed, nCols = _matrix.m_nColumnsUsed;
		int prevKeyElementValue = 1;
		//Перебираем строки матрицы
		for (int i = 0; i < nRows; i++) {

			//Если в матрице строк больше, чем столбцов - считаем оставшиеся строки равными нулю
			if ( i > nCols) {
				for (int j = 0; j < nCols; j++)
					_matrix.m_pData[i][j] = 0;

				continue;
			}

			//Определяем строку с максимальным по модулю i-м элементом, где i - позиция элемента в строке
			int maxElementRowIndex = i;
			double maxElementValue = std::abs(_matrix.m_pData[i][0]);

			for (int j = i + 1; j < nRows; j++) {
				double value = std::abs(_matrix.m_pData[j][i]);
				if (value > maxElementValue) {
					maxElementValue = value;
					maxElementRowIndex = j;
				}
			}

			//Строку с максимальным по модулю элементом подтягиваем выше
			if (maxElementRowIndex != i) {
				SwapRows (_matrix, i, maxElementRowIndex);
			}

			//Сохраняем ключевой элемент
			const double keyElementValue = _matrix.m_pData[i][i];

			//Вычитаем из оставшихся строк текущую (i-ю) строку:
			for (int j = i + 1; j < nRows; j++) {
				const double jPosElementValue =  _matrix.m_pData[j][i];

				MultiplyRow ( _matrix, j, keyElementValue );
				SumRows ( _matrix, j, i, ( jPosElementValue * -1 ) );
				DivideRow ( _matrix, j, prevKeyElementValue );
			}

			prevKeyElementValue = keyElementValue;
		}

	}

} //end of RealMatrix namespace

