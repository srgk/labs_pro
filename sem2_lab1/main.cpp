#include <stdio.h>

double f(double _var) {
	double result = 1;
	const int MULT_NUMBER = 5;
	for (int i = 1; i <= MULT_NUMBER; i++) {
		result *= (_var - i);
	}
	return result;
}

double proxy_func1(double _var) {
	return f(_var);
}

double proxy_func2(const double * _var) {
	return f(*_var);
}

double proxy_func3(const double & _var) {
	return f(_var);
}

int main() {
	double y, a2;
	double x1, x2 = 3.14, x3 = 8;
	printf("Input a2 variable: ");
	scanf("%lf", &a2);

	x1 = a2 + 5;
	y = proxy_func1(x1) * proxy_func2(&x2) * proxy_func3(x3);

	printf("Your expression is: y = f(%.3lf) * f(%.3lf) / f(%.3lf) \n", x1, x2, x3);

	printf("and your y is: %lf \n", y);

	return 0;
}
