/*
 * student_record_vector.cpp
 *
 *  Created on: 1 мая 2015
 *      Author: strangeman
 */

#include "student_record_vector.hpp"
#include "parsers.hpp"
#include <cstring>
#include <iostream>
#include <cassert>

namespace StudentRecord {

void RecVectorInit ( RecVector & _vector, int _allocatedSize )
{
    _vector.m_pData      = new Record[ _allocatedSize ];
    _vector.m_nAllocated =  _allocatedSize;
    _vector.m_nUsed      = 0;
}


void RecVectorDestroy ( RecVector & _vector )
{
    for ( int i = 0; (i < _vector.m_nAllocated ) && _vector.m_pData[ i ].m_isInitiated; i++ )
        RecordDestroy ( _vector.m_pData[ i ] ) ;

    delete[] _vector.m_pData;
}


void RecVectorClear ( RecVector & _vector )
{
    _vector.m_nUsed = 0;
}


bool RecVectorIsEmpty ( const RecVector & _vector )
{
    return _vector.m_nUsed == 0;
}


void RecVectorGrow ( RecVector & _vector )
{
    int nAllocatedNew = _vector.m_nAllocated * 2;
    Record * pNewData = new Record[ nAllocatedNew ];

    memcpy( pNewData, _vector.m_pData, sizeof( Record ) * _vector.m_nAllocated );

    delete[] _vector.m_pData;
    _vector.m_pData = pNewData;

    _vector.m_nAllocated = nAllocatedNew;
}


void RecVectorPushBack ( RecVector & _vector, const Record & _data )
{
	int destIndex = _vector.m_nUsed++;

    if ( _vector.m_nUsed == _vector.m_nAllocated )
        RecVectorGrow( _vector );

    if ( ! _vector.m_pData[destIndex].m_isInitiated )
    	RecordInit (_vector.m_pData[destIndex]);

    RecordCopy (_vector.m_pData[destIndex], _data);
}



void RecVectorPopBack ( RecVector & _vector )
{
    assert( ! RecVectorIsEmpty( _vector ) );

    -- _vector.m_nUsed;
}


void RecVectorReadTillNewline ( RecVector & _vector, std::istream & _stream )
{
	Record temp;
	RecordInit (temp);
    while ( true )
    {
        RecordRead ( temp, _stream );
        if ( Parsers::IsEOL(_stream) )
            break;
        else
        	RecVectorPushBack( _vector, temp );

        RecordClear (temp);
    }
}

void RecVectorPrint ( const RecVector & _vector, std::ostream & _stream )
{
    for ( int i = 0; i < _vector.m_nUsed; i++ )
       RecordPrint (_vector.m_pData[i], _stream);

    _stream << "\n";
}


void RecVectorInsertAt ( RecVector & _vector, int _position, const Record & _data )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    int newUsed = _vector.m_nUsed + 1;
    if ( newUsed > _vector.m_nAllocated )
        RecVectorGrow( _vector );

    for ( int i = _vector.m_nUsed; i > _position; i-- )
        _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

    if ( ! _vector.m_pData[_vector.m_nUsed].m_isInitiated )
    	RecordInit (_vector.m_pData[_vector.m_nUsed]);

    RecordCopy (_vector.m_pData[_vector.m_nUsed], _data);

    _vector.m_nUsed = newUsed;
}


void RecVectorDeleteAt ( RecVector & _vector, int _position )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    Record temp;
    RecordClear (temp);
    temp = _vector.m_pData[_position];

    for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
        _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

    -- _vector.m_nUsed;

    _vector.m_pData[_vector.m_nUsed] = temp;
}

} //end of StudentRecord namespace


