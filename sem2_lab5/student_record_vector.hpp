/*
 * student_record_vector.hpp
 *
 *  Created on: 1 мая 2015
 *      Author: strangeman
 */

#ifndef STUDENT_RECORD_VECTOR_HPP_
#define STUDENT_RECORD_VECTOR_HPP_

#include "student_record.hpp"

namespace StudentRecord {

	struct RecVector {
		Record * m_pData;
		int m_nUsed, m_nAllocated;
		int m_nOfGrades;
	};

	void RecVectorInit ( RecVector & _vector, int _allocatedSize = 10 );

	void RecVectorDestroy ( RecVector & _vector );

	void RecVectorClear ( RecVector & _vector );

	bool RecVectorIsEmpty ( const RecVector & _vector );

	void RecVectorPushBack ( RecVector & _vector, const Record & _data );

	void RecVectorPopBack ( RecVector & _vector );

	void RecVectorInsertAt ( RecVector & _vector, int _position, const Record & _data );

	void RecVectorDeleteAt ( RecVector & _vector, int _position );

	void RecVectorReadTillNewline ( RecVector & _vector, std::istream & _stream );

	void RecVectorPrint ( const RecVector & _vector, std::ostream & _stream );

} //end of StudentRecord namespace


#endif /* STUDENT_RECORD_VECTOR_HPP_ */
