/*
 * student_record.hpp
 *
 *  Created on: 1 мая 2015
 *      Author: strangeman
 */

#ifndef STUDENT_RECORD_HPP_
#define STUDENT_RECORD_HPP_

#include "integer_vector.hpp"
#include "char_vector.hpp"
#include <iostream>

namespace StudentRecord {

	struct Record {
		CharVector::ChVector m_name;
		IntegerVector::IntVector m_grades;
		bool m_isInitiated = false;
	};

	void RecordInit (Record & _record);

	void RecordDestroy (Record & _record);

	void RecordRead (Record & _record, std::istream & _stream);

	void RecordPrint (const Record & _record, std::ostream & _stream);

	void RecordClear (Record & _record);

	void RecordCopy (Record & _dest, const Record & _source);


} //end of StudentRecord namespace


#endif /* STUDENT_RECORD_HPP_ */
