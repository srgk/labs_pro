/*
 * main.cpp
 *
 *  Created on: 1 мая 2015
 *      Author: strangeman
 */
/*
 * 11. Створити  файл, що містить
 *  список студентів з їх оцінками за результатами сесії.
 *   Забезпечити читання файлу і видачу прізвищ
 *   студентів, що мають середній  бал  не  нижче  заданого.
 */

#include <fstream>
#include "student_record_vector.hpp"

int GetAverageGrade (const StudentRecord::Record & _record) {
	int sum = 0;

	for (int i = 0; i < _record.m_grades.m_nUsed; i++)
		sum += _record.m_grades.m_pData[i];

	return sum / _record.m_grades.m_nUsed;
}

void PrintRecordsByAverageGrade (const StudentRecord::RecVector & _vector, int _avGrade, std::ostream & _stream) {
    for ( int i = 0; i < _vector.m_nUsed; i++ )
    	if ( GetAverageGrade ( _vector.m_pData[i] ) >= _avGrade)
    		RecordPrint (_vector.m_pData[i], _stream);

}

int main (int _argc, char** _argv) {
	// Проверяем количество аргументов
	if (_argc < 2) {
		std::cout << "You must to specify a path to file with information about the students as the first argument. But you didn't it.\nExiting...\n";
		return -1;
	}

	// Пытаемся открыть файлы на чтение и запись
	std::ifstream sourceFile(_argv[1]);

	// Проверяем, удалось ли получить доступ к файлам и открыть их
	if ( ! sourceFile.is_open() ) {
		std::cout << "Unable to open your source file. Does it exist?\nExiting...\n";
		return -2;
	}

	StudentRecord::RecVector list;
	StudentRecord::RecVectorInit (list);

	StudentRecord::RecVectorReadTillNewline(list, sourceFile);

	std::cout << "Input average grade for filtering of student records: ";
	int averageGrade;

	std::cin >> averageGrade;

	std::cout << "These records fits:\n";
	PrintRecordsByAverageGrade (list, averageGrade, std::cout);
	//StudentRecord::RecVectorPrint (list,std::cout);
	StudentRecord::RecVectorDestroy (list);

	sourceFile.close();
	return 0;
}
