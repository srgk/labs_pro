/*
 * main.cpp
 *
 *  Created on: 18 апр. 2015
 *      Author: strangeman
 */

//11. Видалити з тексту усі слова, що починаються з літер, які задаються в рядку запиту.

#include <iostream>
#include "char_vector.hpp"
#include "text_manipulations.hpp"

int main () {
	std::cout << "Input \"bad\" letters and press [ENTER]: ";
	CharVector::ChVector badLetters;
	CharVector::Init (badLetters);
	CharVector::GetUntil (badLetters, std::cin, "\n");

	std::cout << "Input your text and press [Ctrl+Z]:\n";
	//CharVector::ChVector rawText;
	//CharVector::Init (rawText);
	//CharVector::GetUntil (rawText, std::cin, "\0");

	CharVector::ChVector cleanedText;
	CharVector::Init (cleanedText);
	//Text::CleanBySymbol (rawText, cleanedText, badLetters);
	Text::CleanBySymbol (std::cin, cleanedText, badLetters);

	std::cout << "\nCleaned text:\n";
	CharVector::Print (cleanedText, std::cout, '\0');

	CharVector::Destroy (badLetters);
	//CharVector::Destroy (rawText);
	CharVector::Destroy (cleanedText);

	return 0;
}
