/*
 * text_manipulations.hpp
 *
 *  Created on: 18 апр. 2015
 *      Author: strangeman
 */

#ifndef TEXT_MANIPULATIONS_HPP_
#define TEXT_MANIPULATIONS_HPP_

#include <iostream>
#include "char_vector.hpp"

namespace Text {

using CharVector::ChVector;

void CleanBySymbol (std::istream & _stream, ChVector & _destContent, ChVector & _startingSymbols);

void CleanBySymbol (const ChVector & _sourceContent, ChVector & _destContent, ChVector & _startingSymbols);


} //end of namespace Text

#endif /* TEXT_MANIPULATIONS_HPP_ */
