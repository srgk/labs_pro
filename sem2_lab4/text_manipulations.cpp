/*
 * text_manipulations.cpp
 *
 *  Created on: 18 апр. 2015
 *      Author: strangeman
 */

#include "text_manipulations.hpp"
#include <cassert>
#include <ctype.h>
#include <string.h>

namespace Text {

using CharVector::ChVector;

const char * WORD_TERMINATORS = " \n\t";

void MakeCharVectorLowercase (ChVector & _vector) {
	for (int i = 0; i < _vector.m_nUsed; i++)
		_vector.m_pData[i] = tolower(_vector.m_pData[i]);
}

bool IsCharIncluded (const char _ch, ChVector & _charPattern) {
	for (int i = 0; i < _charPattern.m_nUsed; i++)
		if ( _ch == _charPattern.m_pData[i] )
			return true;

	return false;
}

bool IsCharIncluded (const char _ch, const char * _charPattern) {
	const int nOfChars = strlen(_charPattern);

	for (int i = 0; i < nOfChars; i++)
		if ( _ch == _charPattern[i] )
			return true;

	return false;
}

void CleanBySymbol (std::istream & _stream, CharVector::ChVector & _destContent, CharVector::ChVector & _startingSymbols) {
	char buf;
	MakeCharVectorLowercase(_startingSymbols);

	while ( _stream.get(buf) ) {

		//IgnoreWord
		if ( IsCharIncluded (tolower(buf), _startingSymbols) ) {
			while ( ! IsCharIncluded (buf, WORD_TERMINATORS) )
				if (! _stream.get(buf) )
					break;

			if ( buf == '\n' )
				CharVector::PushBack ( _destContent, buf );

			continue;
		}

		//SaveWord
		while ( ! IsCharIncluded ( buf, WORD_TERMINATORS ) ) {
			CharVector::PushBack ( _destContent, buf );
			if (! _stream.get(buf) )
				break;
		}

		CharVector::PushBack ( _destContent, buf );
	}

}

void CleanBySymbol (const CharVector::ChVector & _srcContent, CharVector::ChVector & _destContent, CharVector::ChVector & _startingSymbols) {
	char buf;
	MakeCharVectorLowercase(_startingSymbols);

	for (int i = 0; i < _srcContent.m_nUsed; i++) {
		buf = _srcContent.m_pData[i];

		//IgnoreWord
		if ( IsCharIncluded (tolower(buf), _startingSymbols) ) {
			while ( ! IsCharIncluded (buf, WORD_TERMINATORS) || (i >= _srcContent.m_nUsed) )
				buf = _srcContent.m_pData[++i];

			if ( buf == '\n' )
				CharVector::PushBack ( _destContent, buf );

			continue;
		}

		//SaveWord
		while ( ! IsCharIncluded ( buf, WORD_TERMINATORS ) || (i >= _srcContent.m_nUsed) ) {
			CharVector::PushBack ( _destContent, buf );
			buf = _srcContent.m_pData[++i];
		}

		CharVector::PushBack ( _destContent, buf );
	}

}


} //end of namespace Text
